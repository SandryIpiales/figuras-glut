package org.seipiales;


import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;


/**
 * EjercicioGlu.java 
 * author: Sandra Ipiales
 * Solidos con la Libreria GLU
 */
public class EjercicioGlu implements GLEventListener {
    
    static float x,y;
    static float angulo = 0;
    float a;
   
    static  Frame frame = new Frame("Solidos con Libreria GLU");
    
    public static void main(String[] args) {
       
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new EjercicioGlu());
        frame.add(canvas);
        frame.setSize(880,640);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
    }

    public void init(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.84f, 0.84f, 0.84f, 0.0f); //Fondo
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f, h, 1.0, 20.0);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();
        GLUT glut = new GLUT();
        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();

        gl.glTranslated(0.0f, 0.0f, -15.0f);
        GLUquadric quad;
        quad =glu.gluNewQuadric();
        glu.gluQuadricDrawStyle(quad,GLU.GLU_FILL);
        
        //Icosaedro 
          gl.glPushMatrix();
          gl.glColor3f(0.93f, 0.2527f, 0.0465f); //Agregamos color
          gl.glTranslatef(-7.0f,4.5f, 0.0f);
          gl.glRotatef(angulo, -1, 0, 0);
          glut.glutSolidIcosahedron();
          gl.glPopMatrix();
           
        //Icosaedro con malla)
          gl.glPushMatrix();
          gl.glTranslatef(-7.0f,2.5f, 0.0f);
          gl.glRotatef(angulo, 0, -1, 0);
          glut.glutWireIcosahedron();
          gl.glPopMatrix();
          
        //Dodecaedro
          gl.glPushMatrix();
          gl.glColor3f(0.39f, 0.2028f, 0.2496f);//Agregamos color
          gl.glTranslatef(-4.5f,4.5f, 0.0f);
          gl.glScalef(0.6f,0.6f, 0.6f);
          gl.glRotatef(angulo, -1, 0, 0);
          glut.glutSolidDodecahedron();
          gl.glPopMatrix();
        //Dodecaedro (malla)
          gl.glPushMatrix();
          gl.glColor3f(0.39f, 0.2028f, 0.2496f);//Agregamos color
          gl.glTranslatef(-4.5f,2.5f, 0.0f);
          gl.glScalef(0.6f,0.6f, 0.6f);
          gl.glRotatef(angulo, 0, -1, 0);
          glut.glutWireDodecahedron();
          gl.glPopMatrix();
          
        //Dona
          gl.glPushMatrix();          
          gl.glColor3f(0.84f, 0.4794f, 0.3696f); //Agregamos color
          gl.glTranslatef(-1.0f,4.5f, 0.0f);
          gl.glScaled(0.7, 0.7, 0.7);
          gl.glRotatef(angulo, -1, 0, 0);
          glut.glutSolidTorus(0.5,1, 50, 50);
          gl.glPopMatrix();
        //Dona (malla)
          gl.glPushMatrix();
          gl.glColor3f(0.84f, 0.4794f, 0.3696f); //Agregamos color
          gl.glTranslatef(-1.0f,2.3f, 0.0f);
          gl.glScaled(0.7, 0.7, 0.7);
          gl.glRotatef(angulo, 0, -1, 0);
          glut.glutWireTorus(0.5,1, 50, 50);
          gl.glPopMatrix();
          
        //Disco
          gl.glPushMatrix();
          gl.glColor3f(0.42f, 0.203f, 0.0f );//Agregamos color
          gl.glTranslatef(1.5f,4.5f, 0.0f);
          gl.glRotatef(angulo, 1, 0, 0);
          gl.glRotatef(angulo, 0, -1, 0);
          glu.gluDisk(quad, 0.5, 1, 50, 50);
          gl.glPopMatrix();
        //Disco Parcial
          gl.glPushMatrix();
          gl.glColor3f(0.42f, 0.203f, 0.0f );//Agregamos color
          gl.glTranslatef(1.5f,2.5f, 0.0f);
          gl.glRotatef(angulo, -1, 0, 0);
          gl.glRotatef(angulo, 0, 1, 0);
          glu.gluPartialDisk(quad, 0.5, 1, 50, 50, 90, 180);
          gl.glPopMatrix();
           
        //RomboDodecaedro 
          gl.glPushMatrix();
          gl.glColor3f(1.0f, 0.7667f, 0.0f); //Agregamos color
          gl.glTranslatef(4.0f,4.5f, 0.0f);
          gl.glRotatef(angulo, 0, 1, 0);
          glut.glutSolidRhombicDodecahedron();
          gl.glPopMatrix();
        //RomboDodecaedro (malla)
          gl.glPushMatrix();
          gl.glColor3f(1.0f, 0.7667f, 0.0f); //Agregamos color
          gl.glTranslatef(4.0f,2.5f, 0.0f);
          gl.glRotatef(angulo, 1, 0, 0);
          glut.glutWireRhombicDodecahedron();
          gl.glPopMatrix();    
           
           
        //Esfera solida          
          gl.glPushMatrix();
          gl.glColor3f(0.5f, 0.0f, 0.225f); //Agregamos color
          gl.glTranslatef(-7.0f,0.0f, 0.0f);
          gl.glRotatef(angulo, -1, 0, 0);
          glut.glutSolidSphere(1, 30, 30);
          gl.glPopMatrix();
        //Esfera (malla)
          gl.glPushMatrix();
          gl.glColor3f(0.5f, 0.0f, 0.225f); //Agregamos color
          gl.glTranslatef(-7.0f,-2.5f, 0.0f);
          gl.glRotatef(angulo, 0, -1, 0);
          glut.glutWireSphere(1, 30, 30);
          gl.glPopMatrix();           
       
        //Cono solido
           gl.glPushMatrix();
           gl.glColor3f(0.78f, 0.4488f, 0.0156f); //Agregamos color
           gl.glTranslatef(-4.5f,0.0f, 0.0f);
           gl.glRotatef(angulo, 1, 0, 0);
           gl.glRotatef(angulo, 0, -1, 0);
           glut.glutSolidCone(1, 1, 50, 50);
           gl.glPopMatrix();
        //Cono (malla)
           gl.glPushMatrix();
           gl.glColor3f(0.84f, 0.4794f, 0.3696f); //Agregamos color
           gl.glTranslatef(-4.5f,-2.5f, 0.0f);
           gl.glRotatef(angulo, -1, 0, 0);
           gl.glRotatef(angulo, 0, 1, 0);
           glut.glutWireCone(1,1, 50, 50);
           gl.glPopMatrix();           
           
        //Cubo
           gl.glPushMatrix();
           gl.glColor3f(0.312f, 0.39f, 0.195f);//Agregamos color
           gl.glTranslatef(-1.0f,-0.2f, 0.0f);
           gl.glRotatef(angulo, 0, 1, 0);
           glut.glutSolidCube(1.5f);
           gl.glPopMatrix();
        //Cubo(malla)
           gl.glPushMatrix();
           gl.glColor3f(0.52f, 0.65f, 0.325f);//Agregamos color
           gl.glTranslatef(-1.0f,-2.5f, 0.0f);
           gl.glRotatef(angulo, 1, 0, 0);
           glut.glutWireCube(1.5f);
           gl.glPopMatrix();

           
        //Tetraedro 
           gl.glPushMatrix();
           gl.glColor3f(0.4136f, 0.47f, 0.47f); //Agregamos color
           gl.glTranslatef(1.5f,-0.2f, 0.0f);
           gl.glRotatef(45, 0, 1, 0);
           gl.glRotatef(angulo, 0, 1, 0);
           glut.glutSolidTetrahedron();
           gl.glPopMatrix();
        //Tetraedro (Malla)
           gl.glPushMatrix();
           gl.glColor3f(0.4136f, 0.47f, 0.47f); //Agregamos color
           gl.glTranslatef(1.5f,-2.5f, 0.0f);
           gl.glRotatef(angulo, 1, 0, 0);
           glut.glutWireTetrahedron();
           gl.glPopMatrix();
          
        //Octaedro 
           gl.glPushMatrix();
           gl.glColor3f(0.5467f, 0.71f, 0.71f); //Celeste
           gl.glTranslatef(4.0f,-0.2f, 0.0f);
           gl.glRotatef(angulo, 0, 1, 0);
           glut.glutSolidOctahedron();
           gl.glPopMatrix();
        //Octaedro (malla)
           gl.glPushMatrix();
           gl.glColor3f(0.5467f, 0.71f, 0.71f); //Celeste
           gl.glTranslatef(4.0f,-2.5f, 0.0f);
           gl.glRotatef(angulo, 1, 0, 0);
           glut.glutWireOctahedron();
           gl.glPopMatrix();
          
        //Tetera
           gl.glPushMatrix();
           gl.glColor3f(0.7f, 0.0f, 0.0933f);
           gl.glTranslatef(7.0f,.0f, 0.0f);
           gl.glRotatef(angulo, 0, 0, 1);
           gl.glRotatef(angulo, 0, 1, 0);
           glut.glutSolidTeapot(1);
           gl.glPopMatrix();       
        //Tetera(malla)
           gl.glPushMatrix();
           gl.glColor3f(0.7f, 0.0f, 0.0933f);
           gl.glTranslatef(7.0f,-2.5f, 0.0f);
           gl.glRotatef(angulo, 0, 0, 1);
           gl.glRotatef(angulo, 1, 0, 0);
           glut.glutWireTeapot(1);
           gl.glPopMatrix();
           
        //Cilindro
           gl.glPushMatrix();
           gl.glColor3f(0.78f, 0.4488f, 0.0156f); //tomate
           gl.glTranslatef(7.0f,4.5f, 0.0f);
           gl.glScaled(0.8, 0.8, 0.8);
           gl.glRotatef(angulo, -1, 0, 0);
           gl.glRotatef(angulo, 0, -1, 0);
           glu.gluCylinder(quad, 1, 1, 2, 36,36);
           gl.glPopMatrix();
        //Cilindro (malla)
           gl.glPushMatrix();
           gl.glColor3f(0.93f, 0.2527f, 0.0465f); //Rosa fuerte
           gl.glTranslatef(7.0f,2.5f, 0.0f);
           gl.glScaled(0.8, 0.8, 0.8);
           gl.glRotatef(angulo, 1, 0, 0);
           gl.glRotatef(angulo, 0,1, 0);
           glut.glutWireCylinder(1, 2, 50, 50);
        gl.glPopMatrix();
        gl.glFlush();
        angulo+= 0.5;
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }
}


